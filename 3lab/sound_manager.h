#ifndef SOUND_MANAGER_H
#define SOUND_MANAGER_H
#include <QSound>
#include <QMediaPlayer>

class sound_manager
{
    QMediaPlayer _jump;
    QMediaPlayer _die;
    QMediaPlayer _point;

public:
    sound_manager();
    void play_jump();
    void play_die();
    void play_point();
};

#endif // SOUND_MANAGER_H
