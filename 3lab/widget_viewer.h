#ifndef GRAPHIC_CONTROLLER_H
#define GRAPHIC_CONTROLLER_H
#include "viewer.h"
#include "graphic_obj.h"
#include <vector>
#include <map>
#include <string>
#include "image.h"
#include <QWidget>
#include <QPoint>
#include "possition.h"
#include "qt_keybord_manager.h"
#include <QPixmap>
#include "obj_int.h"


class widget_viewer : public viewer,
        public QWidget{

    std::vector<graphic_obj*> _graphic_objects;
    std::map<images::image, QPixmap> _images;
    qt_keybord_manager *_input;
    obj_int _score;

public:
    void set_score(obj_int &value){_score = value;}
    void set_score(int value){_score = value;}
    widget_viewer(qt_keybord_manager *input);
    ~widget_viewer();
    void add_graphic_obj(graphic_obj *new_obj) override;

protected:
    void paintEvent(QPaintEvent *event) override;
    void keyPressEvent(QKeyEvent *event) override;
private:
    QPoint map_game_pos_to_pixel(const position &game_pos);

    float scaling_width(float width);
    float scaling_height(float height);

};

#endif // GRAPHIC_CONTROLLER_H
