#include "speed.h"

speed::speed(float vx, float vy):
    vx(vx),
    vy(vy)
{

}

speed::speed(const speed &other_speed)
{
    vx = other_speed.vx;
    vy = other_speed.vy;

}

speed speed::operator-(const speed &other)
{
    speed res;

    res.vx = this->vx - other.vx;
    res.vy = this->vy - other.vy;
    return res;

}

speed speed::operator+(const speed &other)
{

    speed res;

    res.vx = this->vx + other.vx;
    res.vy = this->vy + other.vy;
    return res;

}

speed &speed::operator=(const speed &other)
{

    this->vx = other.vx;
    this->vy = other.vy;

    return *this;

}
