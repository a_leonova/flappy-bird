#ifndef OBJ_INT_H
#define OBJ_INT_H
#include <string>


class obj_int
{
    int _value;
public:
    obj_int();
    obj_int(int value);
    obj_int& operator++();
    obj_int& operator++(int);
    obj_int& operator=(const obj_int &other);
    obj_int& operator=(const int value);
    std::string to_string();
};

#endif // OBJ_INT_H
