#ifndef CONTROLLER_RETURN_VALUE_H
#define CONTROLLER_RETURN_VALUE_H

namespace controller_return_value {

    enum controller_return_value{
        succsess,
        end,
        collision
    };
}


#endif // CONTROLLER_RETURN_VALUE_H
