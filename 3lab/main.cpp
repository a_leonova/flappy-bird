#include <iostream>
#include <QApplication>
#include <QTimer>
#include <QPushButton>
#include <QBoxLayout>

#include "controller.h"
#include "physic.h"
#include "widget_viewer.h"
#include "viewer_window.h"
#include "qt_keybord_manager.h"
#include <QFontDatabase>


int main(int argc, char *argv[]){

    QApplication a(argc, argv);
    QFontDatabase::addApplicationFont(":/source/fonts/back_to_1982.ttf");
    QTimer timer;

    physic game_physic;
    qt_keybord_manager game_input;
    widget_viewer *game_viewer = new widget_viewer(&game_input);

    //restart->setFixedSize(50, 50);
    viewer_window window(game_viewer);
    window.resize(350, 700);

    //window.setLayout(mainLayout);

    controller _game_controller(&game_physic, game_viewer, &game_input);


    timer.connect(&timer, &QTimer::timeout, [&timer, &_game_controller, &window, &game_viewer](){
        controller_return_value::controller_return_value return_value = _game_controller.step();
        if (return_value == controller_return_value::collision){
            QHBoxLayout *buttonLayout = new QHBoxLayout;
            QPushButton *restart = new QPushButton("Restart");
            QPushButton *records = new QPushButton("Records");
            buttonLayout->addWidget(restart);
            buttonLayout->addWidget(records);

            game_viewer->setLayout(buttonLayout);
            window.repaint();
            timer.stop();

            restart->connect(restart, &QPushButton::clicked, [&_game_controller, &timer, &game_viewer]{

                QLayoutItem *child;
                while ((child = game_viewer->layout()->takeAt(0)) != 0) {
                    delete child->widget();
                    delete child;
                }
                  QLayout *buttonLayout = game_viewer->layout();
                  delete buttonLayout;
                game_viewer->setFocus();

                _game_controller.restart();
                timer.start(30);
            });
        }
        else if (return_value == controller_return_value::end){
            window.close();
        }
        else
            window.repaint();
    }
    );



    timer.start(30);
    window.show();

    return a.exec();
}
