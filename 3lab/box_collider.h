#ifndef BOX_COLLIDER_H
#define BOX_COLLIDER_H
#include "collider.h"

class box_collider : public collider
{
    float width;
    float height;
public:
    float get_width(){return width;}
    float get_height(){return height;}
    box_collider();
    box_collider(float n_w, float n_h);
    bool is_collision(collider *other, position &shift) override;

};

#endif // BOX_COLLIDER_H
