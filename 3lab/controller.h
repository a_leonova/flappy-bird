#ifndef CONTROLLER_H
#define CONTROLLER_H
#include "bird.h"
#include "pair_tube.h"
#include "ground.h"
#include "game_datas.h"
#include "iphysic.h"
#include <vector>
#include <ctime>
#include "input_manager.h"
#include "widget_viewer.h"
#include "background.h"
#include "controller_return_value.h"
#include "sound_manager.h"
#include "obj_int.h"

class controller
{
    std::clock_t last_start;
    std::vector<game_logic*> _game_obj;
    bird *_bird;
    pair_tube *_first_pair;
    pair_tube *_second_pair;
    ground *_ground;
    background *_background;

    obj_int _score;

    sound_manager _sound;
    iphysic *_game_physic;
    viewer *_viewer;
    input_manager *_input;
public:
    obj_int get_score() const {return _score;}
    controller(iphysic *game_physic, viewer *game_viewer, input_manager *input);
    ~controller();
    controller_return_value::controller_return_value step();
private:
    void update(float delta_time);
    void check_tube_passing();
public:
    void restart();
};

#endif // CONTROLLER_H
