#include "viewer_window.h"
#include <QResizeEvent>
#include "constants.h"
viewer_window::viewer_window(QWidget *viewer, QWidget *parent) : QMainWindow(parent)
{
    setCentralWidget(viewer);
}

void viewer_window::resizeEvent(QResizeEvent *ev)
{
    ev->accept();
    float ratio = world_bounds::distance_between_bottom_top/ world_bounds::distance_between_left_right_borders;
    resize(ev->size().height() / ratio, ev->size().height());
}

