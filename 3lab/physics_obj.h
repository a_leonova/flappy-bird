#ifndef PHYSICS_OBJ_H
#define PHYSICS_OBJ_H
#include "collider.h"
#include "possition.h"
#include "speed.h"
#include <vector>
#include "graphic_obj.h"

class physics_obj : public graphic_obj
{
protected:
    position pos;
    speed sp;
    bool is_gravity;
    collider* my_collider;

public:

    position get_pos() const override {return pos;}
    speed get_speed(){return sp;}
    bool get_gravity() const {return is_gravity;}
    float get_rotation() const override {return 0;}
    collider* get_collider() const {return my_collider;}

    void set_sp(speed n_sp) {sp = n_sp;}
    void set_pos(position n_pos) {pos = n_pos;}
    physics_obj(position n_pos, speed n_sp);
    virtual ~physics_obj() = 0;
};

#endif // PHYSICS_OBJ_H
