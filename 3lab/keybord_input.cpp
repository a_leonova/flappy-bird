#include "keybord_input.h"
#include <conio.h>

keybord_input::keybord_input()
{

}

keybord_input::~keybord_input(){

}


bool keybord_input::is_exit(){
    if (!_kbhit())
            return false;

        return _getch() == 27; //ascii of esc
}

bool keybord_input::is_jump(){
    if (!_kbhit())
            return false;

        return _getch() == 32; //ascii of space
}
