#include "possition.h"


position position::operator-(const position &other){
    position res;
    res.y = this->y - other.y;
    res.x = this->x - other.x;
    return res;
}

position position::operator+(const position &other){
    position res;
    res.y = this->y + other.y;
    res.x = this->x + other.x;
    return res;
}

position &position::operator=(const position &other){

    this->x = other.x;
    this->y = other.y;

    return *this;
}

position::position(float x, float y):
    x(x),
    y(y)
{}

position::position(const position &other_pos)
{
    x = other_pos.x;
    y = other_pos.y;
}
