#include "circle_collider.h"
#include "box_collider.h"
#include <cmath>

circle_collider::circle_collider(): r(5)
{}

circle_collider::circle_collider(float n_r):
    r(n_r)
{

}

bool circle_collider::is_collision(collider *other, position &shift){

    box_collider *temp_box = dynamic_cast<box_collider*>(other);
    if (temp_box != nullptr){

        position abs_shift(std::abs((shift.x)), std::abs(shift.y));
        float width = temp_box->get_width();
        float height = temp_box->get_height();

        //there is enough space between centers of rectangle
        //and circle -> no collision
        if(abs_shift.x > (width / 2 + r))
            return false;
        if (abs_shift.y > (height / 2 + r))
            return false;

        //circle crossed side of rectangle
        if (abs_shift.x <= width / 2)
            return true;
        if (abs_shift.y <= height / 2)
            return true;

        //check collision with angle
        float dx = abs_shift.x - width / 2;
        float dy = abs_shift.y - height / 2;

        return(dx * dx + dy * dy <= r * r);
    }

    circle_collider *temp_circul = dynamic_cast<circle_collider*>(other);
    if (temp_circul != nullptr){

        position abs_shift(std::abs((shift.x)), std::abs(shift.y));
        float radius = temp_circul->get_radius();

        if ((radius + r) * (radius + r) >= abs_shift.x * abs_shift.x + abs_shift.y * abs_shift.y)
            return true;
        else
            return false;
    }
    return false;
}
