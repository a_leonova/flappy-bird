#ifndef SPEED_H
#define SPEED_H



struct speed
{
    float vx;
    float vy;
    speed(float vx = 0.0f, float vy = 0.0f);
    speed(const speed &other_speed);
    speed operator-(const speed &other);
    speed operator+(const speed &other);
    speed& operator=(const speed &other);


};
#endif // SPEED_H
