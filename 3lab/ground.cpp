#include "ground.h"

ground::ground(position np, bool use_gr, float width, float height):
    physics_obj(np, speed(0.0, 0.0))
{   //speed is already 0.0; 0.0
    is_gravity = use_gr;
    my_collider = new box_collider(width, height);
    _width = width;
    _height = height;

}

ground::~ground(){
    delete my_collider;
}

void ground::update()
{

}
