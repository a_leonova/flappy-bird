#ifndef QT_KEYBORD_MANAGER_H
#define QT_KEYBORD_MANAGER_H
#include "input_manager.h"
#include <QKeyEvent>

class qt_keybord_manager: public input_manager
{

   QKeyEvent *event;
   bool _exit;
   bool _jump;
public:

    void set_event(QKeyEvent *value){event = value;}
    void check_event();
    bool is_exit() override;
    bool is_jump() override;

    qt_keybord_manager();
protected:




};

#endif // QT_KEYBORD_MANAGER_H
