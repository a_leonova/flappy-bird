#ifndef PHYSIC_H
#define PHYSIC_H
#include <vector>
#include "iphysic.h"
#include "game_datas.h"

class physic: public iphysic
{
    std::vector<physics_obj*> objects;
    static constexpr float g = game_data::game_gravity;
public:
    void add_phys_obj (physics_obj *new_obj) override;
    void update (float t) override;
    bool check_collision(physics_obj *checking_obj) override;

    physic();

};

#endif // PHYSIC_H
