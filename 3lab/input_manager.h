#ifndef INPUT_MANAGER_H
#define INPUT_MANAGER_H


class input_manager{

public:
    virtual bool is_exit() = 0;
    virtual bool is_jump() = 0;
    input_manager() {}
    virtual ~input_manager(){}
};



#endif // INPUT_MANAGER_H
