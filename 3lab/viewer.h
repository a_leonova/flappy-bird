#ifndef VIEWER_H
#define VIEWER_H
#include "image.h"
#include "graphic_obj.h"
#include "obj_int.h"
class viewer
{
public:
    virtual void set_score(obj_int &value) = 0;
    virtual void set_score(int value) = 0;
    virtual void add_graphic_obj(graphic_obj *new_obj) = 0;
    virtual ~viewer(){}

};

#endif // VIEWER_H
