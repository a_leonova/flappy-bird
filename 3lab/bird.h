#ifndef BIRD_H
#define BIRD_H
#include "physics_obj.h"
#include "game_logic.h"
#include "circle_collider.h"
#include "game_datas.h"
#include "sound_manager.h"
class bird :
        public physics_obj,
        public game_logic

{
    static constexpr float max_angle = 30;
    float _radius;
    static constexpr float vy_jump = game_data::bird_jump_speed;
    bool is_jump;
    sound_manager *_sound;

public:
    void set_is_jump(bool value){is_jump = value;}
    bird(position n_pos, speed n_sp,bool use_gravity, float radius, sound_manager *sound);
    float get_rotation() const override;
    images::image get_image() {return images::bird;}
    float get_height() const override {return _radius * 2;}
    float get_width() const override {return _radius * 2;}

    ~bird();
    void update() override;
};

#endif // BIRD_H
