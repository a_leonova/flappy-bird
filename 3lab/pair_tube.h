#ifndef PAIR_TUBE_H
#define PAIR_TUBE_H
#include "tube.h"
#include "game_logic.h"


class pair_tube : public game_logic
{
    tube tube_top;
    tube tube_bottom;
    float width;
    bool gone;

public:
    void set_tube_pos(float pos_x);
    void set_gone(bool value) {gone = value;}
    bool get_gone() {return gone;}
    float get_tubes_posx(){return tube_top.get_pos().x;}
    tube* get_tube_top(){return &tube_top;}
    tube* get_tube_bottom(){return &tube_bottom;}
    pair_tube(float x, float sp_x, float w);
    //~pair_tube();
    void update() override;
private:
    void new_height();
    bool out_of_screen();
};

#endif // PAIR_TUBE_H
