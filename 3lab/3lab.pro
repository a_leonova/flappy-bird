HEADERS += \
    physics_obj.h \
    bird.h \
    tube.h \
    ground.h \
    physic.h \
    input_manager.h \
    keybord_input.h \
    collider.h \
    box_collider.h \
    circle_collider.h \
    pair_tube.h \
    constants.h \
    game_logic.h \
    possition.h \
    controller.h \
    game_datas.h \
    image.h\
    viewer.h\
    graphic_obj.h \
    widget_viewer.h \
    speed.h \
    viewer_window.h \
    iphysic.h \
    qt_keybord_manager.h \
    background.h \
    sound_manager.h \
    controller_return_value.h \
    obj_int.h

SOURCES += \
    physics_obj.cpp \
    bird.cpp \
    tube.cpp \
    ground.cpp \
    physic.cpp \
    main.cpp \
    keybord_input.cpp \
    collider.cpp \
    box_collider.cpp \
    circle_collider.cpp \
    pair_tube.cpp \
    game_logic.cpp \
    possition.cpp \
    controller.cpp\
    graphic_obj.cpp \
    widget_viewer.cpp \
    speed.cpp \
    viewer_window.cpp \
    qt_keybord_manager.cpp \
    background.cpp \
    sound_manager.cpp \
    obj_int.cpp

CONFIG += c++11

QT += widgets\
    core\
    multimedia

FORMS +=

RESOURCES += \
    resource.qrc
