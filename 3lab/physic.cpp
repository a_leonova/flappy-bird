#include "physic.h"
#include <stdexcept>
#include <iostream>


physic::physic()
{

}

void physic::add_phys_obj(physics_obj *new_obj){
    if (new_obj == nullptr){
        throw std::invalid_argument("Nullptr cannot be in objects vector");
        return;
    }
    objects.push_back(new_obj);
}

void physic::update(float t){
    float time = t * 0.001;
    for(auto* obj : objects){


        position new_pos = obj->get_pos();
        new_pos.x += obj->get_speed().vx * time;
        obj->set_pos(new_pos);

        //std::cout << new_pos.x<<std::endl;

        if (obj->get_gravity()){
            speed new_speed = obj->get_speed();
            new_speed.vy -= g * time;

            position new_pos = obj->get_pos();
            new_pos.y += new_speed.vy * time;

            obj->set_pos(new_pos);
            obj->set_sp(new_speed);
        }
    }
}

bool physic::check_collision(physics_obj *checking_obj)
{
    for (physics_obj* other_obj : objects){
        if (other_obj == checking_obj)
            continue;
        position shift = checking_obj->get_pos() - other_obj->get_pos();

        if(checking_obj->get_collider()->is_collision(other_obj->get_collider(), shift)){
             return true;
        }
    }
    return false;
}





