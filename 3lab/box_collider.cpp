#include "box_collider.h"
#include "circle_collider.h"
#include <cmath>

box_collider::box_collider():
    width(10),
    height(15)
{}

box_collider::box_collider(float n_w, float n_h):
    width(n_w),
    height(n_h)
{}


bool box_collider::is_collision(collider *other, position &shift){

    box_collider *temp_box = dynamic_cast<box_collider*>(other);
    if (temp_box != nullptr){
        //that about ground????
        float other_width = temp_box->get_width();
        float other_height = temp_box->get_height();
        if (width / 2 < (shift.x - other_width / 2))
            return false;
        if ((shift.x + other_width/ 2) < (-1 * width / 2))
            return false;
        if (height / 2 < (shift.y - other_height / 2))
            return false;
        if (shift.y + other_height / 2 < (-1 * width / 2))
            return false;
        return true;
    }

    circle_collider *temp_circul = dynamic_cast<circle_collider*>(other);

    if (temp_circul != nullptr){
        float radius = temp_circul->get_radius();
        position abs_shift(std::abs((shift.x)), std::abs(shift.y));

        //there is enough space between centers of rectangle
        //and circle -> no collision
        if(abs_shift.x > (width / 2 + radius))
            return false;
        if (abs_shift.y > (height / 2 + radius))
            return false;

        //circle crossed side of rectangle
        if (abs_shift.x <= width / 2)
            return true;
        if (abs_shift.y <= height / 2)
            return true;

        //check collision with angle
        float dx = abs_shift.x - width / 2;
        float dy = abs_shift.y - height / 2;

        return(dx * dx + dy * dy <= radius * radius);
    }

    return false;
}
