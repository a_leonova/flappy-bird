#ifndef CIRCLE_COLLIDER_H
#define CIRCLE_COLLIDER_H
#include "collider.h"

class circle_collider : public collider
{
   float r;
public:
   float get_radius(){return r;}
    circle_collider();
    circle_collider(float n_r);
    bool is_collision(collider *other, position &shift) override;
};

#endif // CIRCLE_COLLIDER_H
