#ifndef GAME_LOGIC_H
#define GAME_LOGIC_H


class game_logic
{
public:
    virtual ~game_logic() = 0;
    virtual void update() = 0;
};

#endif // GAME_LOGIC_H
