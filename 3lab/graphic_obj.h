#ifndef GRAPHIC_OBJ_H
#define GRAPHIC_OBJ_H
#include "image.h"
#include "possition.h"

class graphic_obj
{

public:
    virtual ~graphic_obj() = 0;
    virtual position get_pos() const = 0;
    virtual float get_height() const = 0;
    virtual float get_width() const = 0;
    virtual float get_rotation() const = 0;
    virtual images::image get_image() = 0;
};

#endif // GRAPHIC_OBJ_H
