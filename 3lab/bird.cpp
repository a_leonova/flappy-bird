#include "bird.h"
#include <QtMath>

bird::bird(position n_pos, speed n_sp,bool use_gravity, float radius, sound_manager *sound):
    physics_obj(n_pos, n_sp)
{
    _sound = sound;
    is_gravity = use_gravity;

    is_jump = false;
    _radius = radius;
    my_collider = new circle_collider(radius);
}

void bird::update()
{
    if (is_jump){
        sp.vy = vy_jump;
        _sound->play_jump();
        is_jump = false;
    }
}

float bird::get_rotation() const
{
    float sp_x = -game_data::pair_tube_speed;
    float sp_y = -sp.vy;
    float angle = qRadiansToDegrees(qAtan2(sp_y, sp_x));
    if (-angle > max_angle)
       angle = -max_angle;
    return angle;
}

bird::~bird(){
    delete my_collider;
}
