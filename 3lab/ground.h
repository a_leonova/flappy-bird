#ifndef GROUND_H
#define GROUND_H
#include "physics_obj.h"
#include "game_logic.h"
#include "box_collider.h"

class ground: public physics_obj, public game_logic
{
    float _height;
    float _width;
public:
    ground(position np, bool use_gr, float width, float height);
    ~ground();
    void update() override;
    images::image get_image() {return images::ground;}
    float get_height() const override {return _height;}
    float get_width() const override {return _width;}

};

#endif // GROUND_H
