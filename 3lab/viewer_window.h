#ifndef VIEWER_WINDOW_H
#define VIEWER_WINDOW_H

#include <QMainWindow>

class viewer_window : public QMainWindow
{
    Q_OBJECT
public:
    explicit viewer_window(QWidget *viewer ,QWidget *parent = nullptr);
protected:
    void resizeEvent(QResizeEvent *ev) override;
};

#endif // VIEWER_WINDOW_H
