#ifndef COLLIDER_H
#define COLLIDER_H
#include "possition.h"

class collider
{
public:
    collider();
    virtual bool is_collision(collider *other, position &shift) = 0;
    virtual ~collider() = 0;

};

#endif // COLLIDER_H
