#include <cstdlib>
#include <ctime>
#include "pair_tube.h"
#include "constants.h"
#include "game_datas.h"

//counting during compilation
static constexpr float tube_height = world_bounds::top - world_bounds::bottom;
static constexpr float hole_height = tube_height / 5;

//rewrite this code :(
void pair_tube::set_tube_pos(float pos_x)
{
    new_height();
    position new_top_pos = tube_top.get_pos();
    new_top_pos.x = pos_x;
    tube_top.set_pos(new_top_pos);

    position new_bot_pos = tube_bottom.get_pos();
    new_bot_pos.x = pos_x;
    tube_bottom.set_pos(new_bot_pos);
}

pair_tube::pair_tube(float x, float sp_x, float w):
    tube_top(position(x, 0), speed(sp_x, 0), w, tube_height, false),
    tube_bottom(position(x, 0), speed(sp_x, 0), w, tube_height, true),
    width(w),
    gone(false)
{
    new_height();
}

void pair_tube::update()
{
    if (out_of_screen()){
        new_height();

        position new_top_pos = tube_top.get_pos();
        new_top_pos.x = world_bounds::right + width / 2;
        tube_top.set_pos(new_top_pos);

        position new_bot_pos = tube_bottom.get_pos();
        new_bot_pos.x = world_bounds::right + width / 2;
        tube_bottom.set_pos(new_bot_pos);

        gone = false;
    }
}

void pair_tube::new_height()
{
    srand(time(0));
    float hole_y = rand() % (unsigned)(0.4 * tube_height) + 0.3 * tube_height;

    position new_top_pos = tube_top.get_pos();
    new_top_pos.y = hole_y + (hole_height + tube_height)/ 2;
    tube_top.set_pos(new_top_pos);

    position new_bot_pos = tube_bottom.get_pos();
    new_bot_pos.y = hole_y - (hole_height + tube_height) / 2;
    tube_bottom.set_pos(new_bot_pos);
}

bool pair_tube::out_of_screen()
{
    bool res =  (tube_bottom.get_pos().x - width / 2) < world_bounds::distance_between_left_right_borders -
            2 * game_data::distance_between_two_pairs;

    return res;
}
