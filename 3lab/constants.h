
#ifndef CONSTANTS_H
#define CONSTANTS_H


namespace world_bounds {
    static constexpr float left = 0;
    static constexpr float right = 100;
    static constexpr float bottom = 0;
    static constexpr float top = 200;
    static constexpr float distance_between_left_right_borders = right - left;
    static constexpr float distance_between_bottom_top = top - bottom;
}



#endif // CONSTANTS_H
