#ifndef KEYBORD_INPUT_H
#define KEYBORD_INPUT_H
#include "input_manager.h"

class keybord_input : public input_manager {

public:
    keybord_input();
    bool is_exit() override;
    bool is_jump() override;

    ~keybord_input() override;
};

#endif // KEYBORD_INPUT_H
