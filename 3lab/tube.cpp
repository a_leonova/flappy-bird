#include "tube.h"

tube::tube(position np, speed ns, float width, float height, bool rotation_180):
    physics_obj(np, ns)
{
    if (rotation_180)
        _rotation = 180;
    else
        _rotation = 0;
    my_collider = new box_collider(width, height);
    is_gravity = false;
    _width = width;
    _height = height;

}

tube::~tube(){
    delete my_collider;
}
