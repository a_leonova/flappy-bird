#ifndef IPHYSIC_H
#define IPHYSIC_H
#include "physics_obj.h"

class iphysic{
public:
    virtual void add_phys_obj (physics_obj *new_obj) = 0;
    virtual void update (float t) = 0;
    virtual bool check_collision(physics_obj *checking_obj) = 0;
    virtual ~iphysic(){}

};

#endif // IPHYSIC_H
