#ifndef TUBE_H
#define TUBE_H
#include "physics_obj.h"
#include <vector>
#include "box_collider.h"

class tube: public physics_obj
{
    float _height;
    float _width;
    float _rotation;

public:
    tube(position np, speed ns, float width, float height, bool rotation_180);
    ~tube();
    images::image get_image() {return images::tube;}

    float get_rotation() const override {return _rotation;}
    float get_height() const override {return _height;}
    float get_width()const override {return _width;}
};

#endif // TUBE_H
