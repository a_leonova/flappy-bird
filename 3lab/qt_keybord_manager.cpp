#include "qt_keybord_manager.h"
#include <QKeyEvent>

void qt_keybord_manager::check_event()
{
    if (event->key() == Qt::Key_Space)
        _jump = true;
    if (event->key() == Qt::Key_Exit)
        _exit = true;
}

bool qt_keybord_manager::is_exit()
{
    bool res = _exit;
    if (_exit){
        _exit = false;
    }
    return res;
}

bool qt_keybord_manager::is_jump()
{
    bool res = _jump;
    if(_jump){
        _jump = false;
    }
    return res;
}

qt_keybord_manager::qt_keybord_manager():
    _exit(false),
    _jump(false)
{

}
