#ifndef BACKGROUND_H
#define BACKGROUND_H
#include "graphic_obj.h"
#include "possition.h"
#include "image.h"

class background : public graphic_obj
{
    position _pos;
    float _height;
    float _width;

public:
    background(position pos, float height,float width);
    position get_pos() const override {return _pos;}
    float get_rotation() const override{return 0;}
    float get_height() const override {return _height;}
    float get_width() const override{return _width;}
    images::image get_image() override {return images::background;}
    ~background(){}
};

#endif // BACKGROUND_H
