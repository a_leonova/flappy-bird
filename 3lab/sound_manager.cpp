#include "sound_manager.h"


sound_manager::sound_manager()
{
    _jump.setMedia(QUrl::fromLocalFile("C:\\papka\\3lab\\source\\sound\\wing.wav"));
    _die.setMedia(QUrl::fromLocalFile("C:\\papka\\3lab\\source\\sound\\hit.wav"));
    _point.setMedia(QUrl::fromLocalFile("C:\\papka\\3lab\\source\\sound\\point.wav"));
}

void sound_manager::play_jump()
{
    _jump.stop();
    _jump.play();

}

void sound_manager::play_die()
{
    _die.play();
}

void sound_manager::play_point()
{
    _point.stop();
    _point.play();
}
