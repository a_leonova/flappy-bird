#include "obj_int.h"

obj_int::obj_int():
    _value(0)
{

}

obj_int::obj_int(int value):
    _value(value)
{

}

obj_int &obj_int::operator++()
{
    ++_value;
    return *this;
}

obj_int &obj_int::operator++(int)
{
    obj_int temp = *this;
    ++(*this);
    return temp;
}

obj_int &obj_int::operator=(const obj_int &other)
{
    if (this == &other)
        return *this;
    _value = other._value;
    return *this;
}

obj_int &obj_int::operator=(const int value)
{
    _value = value;
    return *this;
}

std::string obj_int::to_string()
{
    int dex = 1;
    int value = _value;
    std::string str;

    while(value / dex > 0 && value / (dex * 10) != 0){
        dex *= 10;
        }

    while(dex > 0){
        str.push_back((value / dex) + '0');
        value = value - (value / dex) * dex;
        dex /= 10;
        }

    return str;
}
