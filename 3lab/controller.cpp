#include "controller.h"
#include "game_logic.h"
#include "background.h"

controller::controller(iphysic *game_physic, viewer *game_viewer, input_manager *input):
    last_start(std::clock()),
    _score(0)
{
    _game_physic = game_physic;
    _viewer = game_viewer;
    _input = input;
    _bird = new bird(game_data::bird_pos_start,
                     speed(0.0, 0.0),
                     true,
                     game_data::bird_rad, &_sound);

    _first_pair = new pair_tube(game_data::start_pair_tube_x,
                                game_data::pair_tube_speed,
                                game_data::tube_width);

    _second_pair = new pair_tube(game_data::start_pair_tube_x + game_data::distance_between_two_pairs,
                                 game_data::pair_tube_speed,
                                 game_data::tube_width);

    _ground = new ground(game_data::ground_pos,
                         false,
                         game_data::ground_width,
                         game_data::ground_height);

    _background = new background(game_data::background_pos,
                                 world_bounds::distance_between_bottom_top,
                                 world_bounds::distance_between_left_right_borders);


    _game_physic->add_phys_obj(_bird);
    _game_physic->add_phys_obj(_first_pair->get_tube_top());
    _game_physic->add_phys_obj(_first_pair->get_tube_bottom());
    _game_physic->add_phys_obj(_second_pair->get_tube_top());
    _game_physic->add_phys_obj(_second_pair->get_tube_bottom());
    _game_physic->add_phys_obj(_ground);

    _viewer->add_graphic_obj(_background);
    _viewer->add_graphic_obj(_bird);
    _viewer->add_graphic_obj(_first_pair->get_tube_top());
    _viewer->add_graphic_obj(_first_pair->get_tube_bottom());
    _viewer->add_graphic_obj(_second_pair->get_tube_top());
    _viewer->add_graphic_obj(_second_pair->get_tube_bottom());
    _viewer->add_graphic_obj(_ground);


    _game_obj.push_back(_bird);
    _game_obj.push_back(_first_pair);
    _game_obj.push_back(_second_pair);
    _game_obj.push_back(_ground);
}

void controller::update(float delta_time){

    _bird->set_is_jump(_input->is_jump());

    for (auto obj : _game_obj){
        game_logic *temp = dynamic_cast<game_logic*>(obj);
        if (temp != nullptr){
            temp->update();
        }
    }

    //physic effects on phys.objects
    _game_physic->update(delta_time);
}

void controller::check_tube_passing()
{
    pair_tube *pair;

    for (auto obj : _game_obj){

        pair = dynamic_cast<pair_tube*>(obj);
        if (pair != nullptr && !pair->get_gone() && _bird->get_pos().x > pair->get_tubes_posx()){
            pair->set_gone(true);
            ++_score;
            _sound.play_point();
            _viewer->set_score(_score);
        }
    }
}

void controller::restart()
{
    _bird->set_pos(game_data::bird_pos_start);
    _first_pair->set_tube_pos(game_data::start_pair_tube_x);
    _second_pair->set_tube_pos(game_data::start_pair_tube_x + game_data::distance_between_two_pairs);
    _score = 0;
    _viewer->set_score(0);
    pair_tube *pair;


    for (auto obj : _game_obj){

        pair = dynamic_cast<pair_tube*>(obj);
        if (pair != nullptr){
            pair->set_gone(false);
        }
    }
    last_start = std::clock();
}

controller::~controller()
{
    delete _bird;
    delete _first_pair;
    delete _second_pair;
    delete _ground;
}

controller_return_value::controller_return_value controller::step()
{
    std::clock_t start = std::clock();
    auto ticks = start - last_start;
    float delta_time = ticks / (float)(CLOCKS_PER_SEC / 1000);
    last_start = start;

    update(delta_time);
    check_tube_passing();
    if(_game_physic->check_collision(_bird)){
        _sound.play_die();
        return controller_return_value::collision;
    }

    if(_input->is_exit()){
        return controller_return_value::end;
    }
    return controller_return_value::succsess;
}

