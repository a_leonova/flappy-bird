#include "widget_viewer.h"
#include <QPainter>
#include "constants.h"
#include <QPaintEvent>
#include <QTextEdit>

widget_viewer::widget_viewer(qt_keybord_manager *input):
    _score(0)
{
    _input = input;
    setFocusPolicy(Qt::StrongFocus);
    _images[images::bird] = QPixmap(":/bird.png");
    _images[images::tube] = QPixmap(":/tube.png");
    _images[images::background] = QPixmap(":/background.png");
    _images[images::ground] = QPixmap(":/ground.png");

}

widget_viewer::~widget_viewer()
{
}

void widget_viewer::add_graphic_obj(graphic_obj *new_obj){
    if (new_obj == nullptr){
        throw std::invalid_argument("Nullptr cannot be in objects vector");
        return;
    }
    _graphic_objects.push_back(new_obj);

}

void widget_viewer::paintEvent(QPaintEvent *){

    //QTextEdit screen_score(QString::fromStdString(_score.to_string()));
    QPainter painter(this);
    for (auto obj : _graphic_objects){
        auto central_pos = map_game_pos_to_pixel(obj->get_pos());
        float half_width = scaling_width(obj->get_width()) / 2;
        float half_height = scaling_height(obj->get_height()) / 2;

        QPoint top_left(central_pos.x() - half_width,
                        central_pos.y() - half_height);

        auto image = _images[obj->get_image()];

        QTransform transform;
        transform.rotate(obj->get_rotation());
        transform.scale(2 * half_width / image.width(), 2 * half_height / image.height());
        image = image.transformed(transform);


        painter.drawPixmap(top_left, image);
    }
        QFont font("Back to 1982", 18);
        painter.setPen(QColor(0, 0, 0));
        painter.setFont(font);
        painter.drawText(QPoint(50, 50), QString::fromStdString(_score.to_string()));
}

void widget_viewer::keyPressEvent(QKeyEvent *event)
{
    _input->set_event(event);
    _input->check_event();
}

QPoint widget_viewer::map_game_pos_to_pixel(const position &game_pos){

    int x = scaling_width(game_pos.x - world_bounds::left);
    int y =  this->height() - scaling_height(game_pos.y - world_bounds::bottom);

    return QPoint(x, y);
}

float widget_viewer::scaling_width(float width)
{
    return  width / world_bounds::distance_between_left_right_borders * this->width();
}

float widget_viewer::scaling_height(float height){
    return  height /  world_bounds::distance_between_bottom_top * this->height();
}
