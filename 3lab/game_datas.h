#ifndef GAMES_SIZE_H
#define GAMES_SIZE_H
#include "constants.h"
#include "possition.h"
#include <cmath>
namespace game_data {
    static const position bird_pos_start = position((world_bounds::right) * 0.3,
                                                        (world_bounds::top + world_bounds::bottom) / 2);
    static constexpr float start_pair_tube_x = world_bounds::right;
    static constexpr float pair_tube_speed = -1*30.0f;
    static constexpr float bird_rad = 4.0f;
    static constexpr float tube_width = 15.0f;
    static constexpr float ground_width = world_bounds::distance_between_left_right_borders;
    static constexpr float ground_height = world_bounds::distance_between_bottom_top * 0.15;
    static const position ground_pos(ground_width / 2, ground_height / 2);
    static const position background_pos(world_bounds::distance_between_left_right_borders / 2,
                                             world_bounds::distance_between_bottom_top / 2);
    static constexpr float game_gravity = 200.0f;
    static constexpr float bird_jump_speed = 90.0f;
    //static constexpr float bird_y =
    static constexpr float distance_between_two_pairs = 0.58 * world_bounds::distance_between_left_right_borders;
}
#endif // GAMES_SIZE_H
